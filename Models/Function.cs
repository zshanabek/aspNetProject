using System.Collections.Generic;
namespace universiade2017.Models{
    public class Function
    {
        public int Id{get;set;}

        public string name {get;set;}
        
        public List<Volunteer> Volunteers{get;set;}
    }
}