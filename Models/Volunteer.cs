using System.ComponentModel.DataAnnotations;
using System;
namespace universiade2017.Models{
    public class Volunteer
    {        
        public int ID {get;set;}

        [EmailAddress]    
        [Required]    
        public string Email {get;set;}    

        [DataTypeAttribute(DataType.Password)]
        [StringLengthAttribute(10,MinimumLength = 3)]
        [Required]
        public string Password { get; set; }

        [Required]
        [Display(Name = "First name")]
        public string FirstName {get;set;}
        
        [Required]
        [Display(Name = "Last name")]
        public string LastName {get;set;}

        [Display(Name = "Document ID")]
        [Required]
        public int DocumentId {get;set;}      

        [DataTypeAttribute(DataType.Date)]
        [Display(Name="Your Birth Date")]
        public DateTime BirthDate { get; set; }
    }
}