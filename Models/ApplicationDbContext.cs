using Microsoft.EntityFrameworkCore;
namespace universiade2017.Models
{
    public class ApplicationDbContext:DbContext
    {
         public DbSet<Volunteer> Volunteers{get;set;}
         public DbSet<Zone> Zones{get;set;}
         public DbSet<Function> Functions{get;set;}
         
         protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
         {
            optionsBuilder.UseSqlite("Filename=./universiade2017.db");
         }
    }
}