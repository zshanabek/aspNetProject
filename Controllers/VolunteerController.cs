using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using universiade2017.Models;
using System.Text.Encodings.Web;
namespace universiade2017.Controllers
{
    public class VolunteerController : Controller
    {
        private ApplicationDbContext ctx = new ApplicationDbContext();
        public IActionResult Index()
        {            
            var volunteer = ctx.Volunteers;
            return View(volunteer.ToList());
        }

        public IActionResult AddVolunteer()
        {   
            Volunteer volunteer = new Volunteer();
            return View(volunteer);
        }
        [ValidateAntiForgeryTokenAttribute]
        public IActionResult Register(Volunteer volunteer)
        {
            if(ModelState.IsValid)
            {
                ctx.Add(volunteer);
                ctx.SaveChanges();
                return View("AddVolunteer", volunteer);   
            }  
            ModelState.AddModelError("","Some Fields Are Filled In A Wrong Way");                 
            return View("AddVolunteer", volunteer);
        }
        public IActionResult EditVolunteer(int id)
        {   
            var volunteer = ctx.Volunteers.Find(id);
            return View(volunteer);
        }
        [HttpPostAttribute]
        public IActionResult EditVolunteer(Volunteer vol)
        {   
            if(ModelState.IsValid)
            {
                ctx.Update(vol);
                ctx.SaveChanges();
                Console.WriteLine("Successfully updated");
                return RedirectToAction("Index");
            }
            
            return View(vol);
        }
        public IActionResult DeleteVolunteer(int id)
        {  
            Volunteer volunteer = ctx.Volunteers.Find(id);
            ctx.Volunteers.Remove(volunteer);
            ctx.SaveChanges();
            Console.WriteLine("Successfully deleted");             
            return RedirectToAction("Index");
        }
        public IActionResult DetailsVolunteer(int id)
        {   
            Volunteer volunteer = ctx.Volunteers.Find(id);
            return View(volunteer);
        }
    }
}
